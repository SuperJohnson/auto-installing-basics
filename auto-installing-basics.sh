#!/bin/bash

# Written by SuperJohnson

# Variables declaration
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
directory=$PWD
usefull=0
minusefull=0

# check

echo "INTERACTIVE AUTO-INSTALLING SCRIPT FOR DEVELOPPERS USUAL NEEDS"
echo "SUCCESSFULLY TESTED ON DEBIAN 9"
echo "${green}Starting...${reset}"

# Checking if you are a root user
if [ $USER != "root" ]
then
	# Ended script
	echo "${red}You have to execute this script as root"
	echo "Ending script...${reset}"
fi

# Asking for the dev user name
until [ $? -eq 0 ] && [ ${#devuser} -ge 1 ]
do
	echo "${green}Who is the usual developper user on this machine?"
	echo "Note : this question will be asked until you entered a valid name."
	echo "Please enter your usual developer user name then Press Enter : ${reset}"
	read devuser
	id -u $devuser > /dev/null 2>&1
done
echo "${green}Thanks $devuser. This username will be used to configure most of the packages.${reset}"

# Logs folder creation
if [ ! -d $PWD/logs ]
then
    mkdir $PWD/logs
fi
chown -R $devuser:$devuser $PWD/logs

# Logfile begins
begin=$(date +"%Y-%m-%d-%Hh-%Mmn-%Ss")
logfile="logs/log-$begin.log"
printf "INTERACTIVE AUTO-INSTALLING SCRIPT :\r\nStarted at $begin\r\n\r\n" > $logfile

# Log user
printf "GLOBAL :\r\nUser choice : $devuser\r\n" >> $logfile

# Asking for the dev user email
echo "${green}What is the email address related to $devuser?"
echo "Note : you can leave it blank."
echo "Please enter your email : ${reset}"
read devemail
echo "${green}Thanks $devuser. This email will be used to configure most of the packages.${reset}"

# Log email
printf "Email choice : $devemail\r\n" >> $logfile

# Asking for the server IP address
until [[ "$ipv4" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]
do
	echo -e "\n${green}Please enter the IP address of the machine you are using (IPv4 format) : ${reset}"
	read ipv4
done

# Log IP address
printf "Ipv4 choice : $ipv4\r\n" >> $logfile

# Dev or Prod config
echo "${green}Are you in a development environment?"
read -p "Type Y for Yes, anything else for No : ${reset}" -n 1 -r
if [ $REPLY = "Y" ]
then
	# Log dev mode
	printf "Environment : Dev\r\n\r\n" >> $logfile
	env="dev"
else
	# Log prod mode
	printf "Environment : Prod\r\n\r\n" >> $logfile
	env="prod"
fi

# Installing usefull developpers packages
echo -e "\n${green}Do you want to install some usefull developper packages?"
echo "Details : sudo aptitude openssh apt-transport-https lsb-release ca-certificates curl"
read -p "Type Y for Yes, anything else for No : ${reset}" -n 1 -r
if [ $REPLY = "Y" ]
then
	# Installing tools
	usefull=1
	minusefull=1
	echo -e "\n${green}Installing usefull developper packages...${reset}"
	apt-get install -y sudo aptitude openssh-server openssh-client apt-transport-https lsb-release ca-certificates curl

	# Adding user to sudoers
	echo "${green}Adding "$devuser" to sudoers...${reset}"
	adduser $devuser sudo

	# Git config
	echo "${green}Configuring git...${reset}"
	git config --global user.name $devuser
    git config --global user.email $devemail
	echo "${green}Git configuration ended.${reset}"

	# SSH config
	echo "${green}Generating ssh key for $devuser...${reset}"
	if [ ! -d /home/$devuser/.ssh ]
	then
		mkdir /home/$devuser/.ssh
	fi
	read -p "${green}Please enter a passphrase for your SSH key (you can leave it blank) : ${reset}" -r
	echo -e 'y\n\'|ssh-keygen -q -t rsa -N "$REPLY" -f /home/$devuser/.ssh/id_rsa
	chown -R $devuser:$devuser /home/$devuser/.ssh
	sed -i -e 's/root@/'"$devuser"'@/g' /home/$devuser/.ssh/id_rsa.pub
	echo -e "\n${green}SSH configuration ended.${reset}"

	# Log tools
	sshkey=$(</home/dev/.ssh/id_rsa.pub)
	printf "USEFULL DEVELOPPER PACKAGES :\r\nINSTALLED : sudo aptitude openssh apt-transport-https lsb-release ca-certificates curl git\r\n$devuser added to sudoers\r\nGit configured for $devuser with devemail\r\nSSH public key : $sshkey\r\n\r\n" >> $logfile

else
	echo -e "\n${green}Packages not installed.${reset}"

	# Log tools
	printf "USEFULL DEVELOPPER PACKAGES :\r\nNOT INSTALLED\r\n\r\n" >> $logfile
fi

# Installing samba
echo "${green}Do you want to install samba?"
read -p "Type Y for Yes, anything else for No : ${reset}" -n 1 -r
if [ $REPLY = "Y" ]
then
		# Installing samba
        echo -e "\n${green}Installing samba...${reset}"
        apt-get install -y samba

        echo "${green}Please enter a samba server name then press Enter : ${reset}"
        read sambaserver
	
		echo "${green}Please choose a password for your samba $devuser user : ${reset}"
		read sambapass

        # Samba config
        echo "${green}Configuring samba...${reset}"
        if [ -f /etc/samba/smb.conf.back ]
        then
        	cp /etc/samba/smb.conf /etc/samba/smb.conf.last.back
        	cp /etc/samba/smb.conf.back /etc/samba/smb.conf
        else
        	cp /etc/samba/smb.conf /etc/samba/smb.conf.back
    	fi
        (echo $sambapass; echo $sambapass) | smbpasswd -a -s $devuser
        if [ ! -d /var/www ]
		then
			mkdir /var/www
		fi
		chown -R $devuser:$devuser /var/www
		chmod -R 755 /var/www
		cat >> /etc/samba/smb.conf <<EOL
# My shared folder 
   [$sambaserver]
   comment = /var/www
   browseable = yes
   path = /var/www
   read only = no
   directory mask = 755
   create mask = 755
   valid users = $devuser
EOL
		service smbd restart
		echo "${green}Samba configuration ended.${reset}"

		# Log samba
		printf 'SAMBA :\r\nINSTALLED\r\nSamba server : \\\\'"$ipv4\\$sambaserver\r\nSamba user : $devuser\r\nSamba password : $sambapass\r\n" >> $logfile
		if [ -f /etc/samba/smb.conf.last.back ]
        then
        	printf "Last samba configuration backed up at /etc/samba/smb.conf.last.back\r\n" >> $logfile
        fi
        printf "Original samba configuration backed up at /etc/samba/smb.conf.back\r\n\r\n" >> $logfile
else
        echo "${green}Samba not installed.${reset}"

        # Log samba
		printf "SAMBA :\r\nNOT INSTALLED\r\n\r\n" >> $logfile
fi

# Installing PHP
echo "${green}Do you want to install PHP 7.2, PECL and Composer?"
echo "Details : php php-fpm php-gd php-mysql php-cli php-common php-curl php-opcache php-json php-gd php-mcrypt php-mbstring php-xml php-intl php-zip php-dev php-pear composer"
read -p "Type Y for Yes, anything else for No : ${reset}" -n 1 -r
if [ $REPLY = "Y" ]
then
		# Installing PHP
		echo -e "\n${green}Installing PHP 7.2, PECL and Composer...${reset}"
		if [ $usefull = 0 ]
		then
			apt-get install -y aptitude apt-transport-https lsb-release ca-certificates
			minusefull=1
		fi	
		wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
		echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list
		apt-get update
		aptitude install -y php7.2 php7.2-fpm php7.2-gd php7.2-mysql php7.2-cli php7.2-common php7.2-curl php7.2-opcache php7.2-json php7.2-gd php7.2-mbstring php7.2-xml php7.2-intl php7.2-zip php7.2-dev

		# Installing mcrypt for PHP 7.2 (not included anymore)
		apt-get install -y php-pear
		pecl channel-update pecl.php.net
		apt-get install -y libmcrypt-dev libreadline-dev
		pecl install mcrypt-1.0.1

		# PHP config
		echo "${green}PHP 7.2 configuration...${reset}"
		if [ -f /etc/php/7.2/fpm/php.ini.back ]
        then
        	cp /etc/php/7.2/fpm/php.ini /etc/php/7.2/fpm/php.ini.last.back
        	cp /etc/php/7.2/fpm/php.ini.back /etc/php/7.2/fpm/php.ini
        else
        	cp /etc/php/7.2/fpm/php.ini /etc/php/7.2/fpm/php.ini.back
    	fi
		
		# Activating mcrypt in php conf file under the shmop extension declaration
		sed -i -e 's/;extension=shmop/;extension=shmop\'$'nextension=mcrypt.so  ; Custom config/g' /etc/php/7.2/fpm/php.ini

		# Activating errors if Dev
		if [ $env = "dev" ]
		then
			sed -i -e 's/display_errors = Off/display_errors = On  ; Custom config/g' /etc/php/7.2/fpm/php.ini
			sed -i -e 's/display_startup_errors = Off/display_startup_errors = On  ; Custom config/g' /etc/php/7.2/fpm/php.ini
			dev=1
		fi

		# Asking for the upload_max_filesize
		upload_max_filesize=0
		until [ $upload_max_filesize -ge 1 ] && [ $upload_max_filesize -le 50 ]
		do
			echo "${green}Please enter an upload_max_filesize for the PHP configuration (1 to 50M) : ${reset}"
			read upload_max_filesize
		done
		sed -i -e 's/upload_max_filesize = 2M/upload_max_filesize = '"$upload_max_filesize"'M  ; Custom config/g' /etc/php/7.2/fpm/php.ini
		service php7.2-fpm restart
		echo "${green}PHP 7.2 configuration ended.${reset}"

		# Installing Composer
		php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
		php composer-setup.php
		mv composer.phar /usr/local/bin/composer
		php -r "unlink('composer-setup.php');"
		chmod a+x /usr/local/bin/composer

		# Log php
		printf "PHP7.2 :\r\nINSTALLED : php php-fpm php-gd php-mysql php-cli php-common php-curl php-opcache php-json php-gd php-mcrypt php-mbstring php-xml php-intl php-zip php-dev php-pear composer\r\n" >> $logfile
		if [ -z $dev ]
		then
			printf "display_errors = On\r\ndisplay_startup_errors = On\r\n" >> $logfile
		fi
		printf "upload_max_filesize = ${upload_max_filesize}M\r\n" >> $logfile
		if [ -f /etc/php/7.2/fpm/php.ini.last.back ]
        then
        	printf "Last PHP configuration backed up at /etc/php/7.2/fpm/php.ini.last.back\r\n" >> $logfile
        fi
        printf "Original PHP configuration backed up at /etc/php/7.2/fpm/php.ini.back\r\n\r\n" >> $logfile
else
        echo -e "\n${green}PHP 7.2, PECL and Composer not installed.${reset}"

        # Log PHP
        printf "PHP7.2 :\r\nNOT INSTALLED\r\n\r\n" >> $logfile
fi

# Installing MariaDB
echo "${green}Do you want to install MariaDB?"
read -p "Type Y for Yes, anything else for No : ${reset}" -n 1 -r
if [ $REPLY = "Y" ]
then
	# Installing MariaDB
	apt-get install -y mariadb-server mariadb-client

	# MariaDB config
	echo -e "\n${green}MariaDB configuration...${reset}"
	if [ -f /etc/mysql/mariadb.conf.d/50-server.cnf.back ]
    then
    	cp /etc/mysql/mariadb.conf.d/50-server.cnf /etc/mysql/mariadb.conf.d/50-server.cnf.last.back
    	cp /etc/mysql/mariadb.conf.d/50-server.cnf.back /etc/mysql/mariadb.conf.d/50-server.cnf

    	# Asking for old root password
		until [ -n $oldrootpass ] && [ ${#oldrootpass} -ge 1 ]
		do
			echo "${green}Previous configuration detected. Please type your current MySQL root password (cannot be empty) : ${reset}"
			read oldrootpass
		done
    else
    	cp /etc/mysql/mariadb.conf.d/50-server.cnf /etc/mysql/mariadb.conf.d/50-server.cnf.back
	fi

	# Asking for new root password
	until [ -n $rootpass ] && [ ${#rootpass} -ge 1 ]
	do
		echo "${green}Please choose a MySQL root password (cannot be empty) : ${reset}"
		read rootpass
	done

	# Auto-config commands
	if [ -v oldrootpass ]
	then
		sudo mysql -u root -p$oldrootpass --database=mysql -e"USE mysql;DELETE FROM user WHERE user='';UPDATE user SET password=PASSWORD('$rootpass') WHERE user = 'root';UPDATE user SET plugin = '' WHERE user = 'root';FLUSH PRIVILEGES;"
	else
		sudo mysql -u root --database=mysql -e"USE mysql;DELETE FROM user WHERE user='';UPDATE user SET password=PASSWORD('$rootpass') WHERE user = 'root';UPDATE user SET plugin = '' WHERE user = 'root';FLUSH PRIVILEGES;"
	fi
	sed -i -e 's/127.0.0.1/'"$ipv4"'  # Custom config/g' /etc/mysql/mariadb.conf.d/50-server.cnf
	service mariadb restart
	echo "${green}MariaDB configuration ended.${reset}"

	# Log MariaDB
	printf "MariaDB :\r\nINSTALLED\r\nRoot password : $rootpass\r\nListening for IP : $ipv4\r\n" >> $logfile
	if [ -f /etc/mysql/mariadb.conf.d/50-server.cnf.last.back ]
    then
    	printf "Last MariaDB configuration backed up at /etc/mysql/mariadb.conf.d/50-server.cnf.last.back\r\n" >> $logfile
    fi
    printf "Original MariaDB configuration backed up at /etc/mysql/mariadb.conf.d/50-server.cnf.back\r\n\r\n" >> $logfile
else
	echo -e "\n${green}MariaDB not installed.${reset}"

	# Log MariaDB
    printf "MariaDB :\r\nNOT INSTALLED\r\n\r\n" >> $logfile
fi

# Installing Nginx
echo "${green}Do you want to install Nginx?"
read -p "Type Y for Yes, anything else for No : ${reset}" -n 1 -r
if [ $REPLY = "Y" ]
then
	# Installing Nginx
	echo -e "\n${green}Installing Nginx...${reset}"
	wget http://nginx.org/keys/nginx_signing.key
	apt-key add nginx_signing.key
	rm nginx_signing.key

	# Sources modification only if necessary
	if ! grep -q "nginx official repo" "/etc/apt/sources.list"
	then
		echo "" | tee -a /etc/apt/sources.list > /dev/null 2>&1
		echo "# nginx official repo" | tee -a /etc/apt/sources.list > /dev/null 2>&1
		echo "deb http://nginx.org/packages/mainline/debian/ stretch nginx" | tee -a /etc/apt/sources.list > /dev/null 2>&1
		echo "deb-src http://nginx.org/packages/mainline/debian/ stretch nginx" | tee -a /etc/apt/sources.list > /dev/null 2>&1
		apt-get update
	fi
	apt-get install -y nginx-extras

	# Nginx config
	echo "${green}Nginx configuration...${reset}"

	if [ -f /etc/nginx/nginx.conf.back ]
    then
    	cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.last.back
    else
    	cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.back
	fi
	rm /etc/nginx/nginx.conf
	cp ressources/nginx/nginx.conf /etc/nginx/nginx.conf
	chmod 644 /etc/nginx/nginx.conf
	chown root:root /etc/nginx/nginx.conf
	cp ressources/nginx/php.conf /etc/nginx/php.conf
	chmod 644 /etc/nginx/php.conf
	chown root:root /etc/nginx/php.conf
	chown -R $devuser:www-data /var/www
	service nginx restart
	echo "${green}Nginx configuration ended.${reset}"

	# Log Nginx
	printf "Nginx :\r\nINSTALLED\r\n/etc/apt/sources.list modified to add the Nginx official repo" >> $logfile
	if [ -f /etc/nginx/nginx.conf.last.back ]
    then
    	printf "Last Nginx configuration backed up at /etc/nginx/nginx.conf.last.back\r\n" >> $logfile
    fi
    printf "Original Nginx configuration backed up at /etc/nginx/nginx.conf.back\r\n\r\n" >> $logfile

else
	echo -e "\n${green}Nginx not installed.${reset}"

	# Log Nginx
    printf "Nginx :\r\nNOT INSTALLED\r\n\r\n" >> $logfile
fi


# Installing Drush (depends on PHP)
if [ -f /etc/php/7.2/fpm/php.ini ]
then
	echo "${green}Do you want to install Drush?"
	read -p "Type Y for Yes, anything else for No : ${reset}" -n 1 -r
	if [ $REPLY = "Y" ]
	then
		# Installing Drush
		php -r "readfile('https://github.com/drush-ops/drush/releases/download/8.1.16/drush.phar');" > drush
		chmod +x drush
		mv drush /usr/local/bin/
		echo "${green}Drush configuration ended.${reset}"

		# Log Drush
		printf "Drush :\r\nINSTALLED\r\n\r\n" >> $logfile
	else
		echo -e "\n${green}Drush not installed.${reset}"

		# Log Drush
	    printf "Drush :\r\nNOT INSTALLED\r\n\r\n" >> $logfile
	fi
fi

# Installing PHPMyAdmin (depends on MariaDB, Nginx, PHP, Composer)
if [ -f /etc/mysql/mariadb.conf.d/50-server.cnf ] && [ -f /etc/nginx/nginx.conf ] && [ -f /etc/php/7.2/fpm/php.ini ]
then
	echo "${green}Do you want to install PHPMyAdmin?"
	read -p "Type Y for Yes, anything else for No : ${reset}" -n 1 -r
	if [ $REPLY = "Y" ]
	then
		# Installing PHPMyAdmin
		echo -e "\n${green}Installing PHPMyAdmin...${reset}"
		if [ -d /usr/share/phpmyadmin ]
		then
			rm -R /usr/share/phpmyadmin
		fi
		git clone https://github.com/phpmyadmin/phpmyadmin /usr/share/phpmyadmin
		cd /usr/share/phpmyadmin
		git checkout .
		git pull
		git checkout STABLE
		composer update --no-dev
		mkdir tmp
		chown www-data:www-data tmp
		chmod 700 tmp
		cd directory=$PWD
		cp /usr/share/phpmyadmin/config.sample.inc.php /usr/share/phpmyadmin/config.inc.php
		sed -i -e "s/$cfg\['blowfish_secret'\] = '';/$cfg\['blowfish_secret'\] = '00000000000000000000000000000000';/g" /usr/share/phpmyadmin/config.inc.php
		sed -i -e "s/$cfg\['compress'\] = false;/$cfg\['compress'\] = true;/g" /usr/share/phpmyadmin/config.inc.php

		# Asking for the second-level domain
		until [[ "$secdomain" =~ ^([a-z](-?[a-z0-9])*)$ ]]
		do
			echo "${green}What is the second-level domain for PHPMyAdmin?"
			echo "Note : only lowercase letters, numbers and dashes are allowed. This question will be asked until you entered a valid name."
			read secdomain
		done

		# Asking for the top-level domain
		until [[ "$topdomain" =~ ^[a-zA-Z]{2,}$ ]]
		do
			echo "${green}What is the top-level domain for PHPMyAdmin?"
			echo "Note : only lowercase letters, at leat two. This question will be asked until you entered a valid name."
			read topdomain
		done

		# Vhost auto-conf
		cp ressources/vhosts/phpmyadmin /etc/nginx/sites-available/phpmyadmin
		chmod 644 /etc/nginx/sites-available/phpmyadmin
		if [ $env = "dev" ]
		then
			vhost="$secdomain.dev.$topdomain"
		else
			vhost="$secdomain.$topdomain"
		fi
		sed -i -e 's/VHOST.EXTENSION/'"$vhost"'/g' /etc/nginx/sites-available/phpmyadmin
		service nginx restart
		echo "${green}PHPMyAdmin configuration ended.${reset}"

		# Log PHPMyAdmin
		printf "PHPMyAdmin :\r\nINSTALLED\r\nVhost available at $vhost and www.$vhost\r\n\r\n" >> $logfile

	else
		echo -e "\n${green}PHPMyAdmin not installed.${reset}"

		# Log Nginx
	    printf "PHPMyAdmin :\r\nNOT INSTALLED\r\n\r\n" >> $logfile
	fi
fi


# Installing Docker

# Installing Symfony (webpack)

# Installing Drupal

# chmod 644 /ressources & /vhosts ?



echo "${green}SCRIPT ENDED, BYE!${reset}"