user www-data;
worker_processes auto;
pid /var/run/nginx.pid;

events {
    worker_connections 1024;
}
http {
    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    # Log path
    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    sendfile on;
    tcp_nodelay on;

    # Less informations about web server
    server_tokens off;

    # Buffers size and max size of requests
    client_body_buffer_size 1k;
    client_max_body_size 8m;
    large_client_header_buffers 1 1K;
    ignore_invalid_headers on;
    
    # Timeout declarations
    client_body_timeout 5;
    client_header_timeout 5;
    keepalive_timeout 65 5;
    send_timeout 5;
    server_name_in_redirect off;
    
    # Page compression except for crappy browsers
    gzip on;
    gzip_comp_level 6;
    gzip_proxied any;
    gzip_vary on;
    gzip_types text/plain text/css application/x-javascript application/json application/xml;
    gzip_disable "MSIE [1-6]\.(?!.*SV1)";

    # Phusion Passenger config (if Gitlab is installed with bundled Nginx off)
    # include /etc/nginx/passenger.conf;
    
    # Max number of connections by client
    limit_req_zone $binary_remote_addr zone=req_limit_per_ip:10m rate=2r/s;
    limit_conn_zone $binary_remote_addr zone=conn_limit_per_ip:10m;

    # Other config files then enabled vhosts included
    include /etc/nginx/conf.d/*.conf;
    include /etc/nginx/sites-enabled/*;
}
